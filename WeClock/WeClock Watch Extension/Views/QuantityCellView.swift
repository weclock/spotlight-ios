import SwiftUI

struct QuantityCellView: View {
	
	public let title: String
	public let unit: String
	public let amount: Double
	public let roundingGuide: RoundingGuide
	
	var amountDisplay: String {
		amount.string(roundedTo: roundingGuide)
	}
	
	private let titleFontSize: CGFloat = 12
	private let amountFontSize: CGFloat = 30
	private let unitFontSize: CGFloat = 15
	
	var body: some View {
		VStack(alignment: .leading, spacing: 0) {
			Text(title)
				.font(Font.system(size: titleFontSize))
			HStack(alignment: .firstTextBaseline, spacing: 5) {
				Text(amountDisplay)
					.font(Font.system(size: amountFontSize))
					.bold()
					.lineLimit(1)
					.layoutPriority(1)
				Text(unit)
					.font(Font.system(size: unitFontSize))
					.minimumScaleFactor(0.5)
					.lineLimit(1)
			}
		}.padding(10)
	}
}
