import SwiftUI

struct CommuteInProgressView: View {
	
	//MARK: Environment
	@EnvironmentObject var commuteManager: CommuteManager
	@Environment(\.presentationMode) var presentationMode
	
	var body: some View {
		VStack(alignment: .center, spacing: 15) {
			Text("Commute in progress.")
				.multilineTextAlignment(.center)
				.lineLimit(4)
				.font(Font.system(size: 25))
			Button(action: {
				self.presentationMode.wrappedValue.dismiss()
			}) {
				Text("I've arrived!")
			}
			.foregroundColor(.black)
			.background(Color.green)
			.cornerRadius(10)
		}
	}
}

