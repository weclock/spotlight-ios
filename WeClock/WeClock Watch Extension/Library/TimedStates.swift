import Combine
import Foundation

class TimedStates<T>: ObservableObject {
	
	//MARK: Definitions
	public typealias IntervalPublisher = Publishers.Autoconnect<Timer.TimerPublisher>
	
	//MARK: Observables
	@Published var currentState: T
	@Published var didFinish: Bool = false
	
	//MARK: Configuration
	private var index: Int = 0
	private var states: [T]
	private var loop: Bool
	private var cancellables = [AnyCancellable]()
	
	//MARK: The actual timer
	public var intervalTimer: IntervalPublisher
	
	//MARK: Initialization
	init(initialState: T, subsequentStates: [T], interval: TimeInterval = 1.0, autostart: Bool = true, loop: Bool = false) {
		self.intervalTimer = Timer.publish(every: interval, on: .current, in: .common).autoconnect()
		self.currentState = initialState
		self.states = subsequentStates
		self.loop = loop
		if autostart {
			subscribe()
		}
	}
	
	init(initialState: T, subsequentStates: [T], intervalTimer: IntervalPublisher, autostart: Bool = true, loop: Bool = false) {
		self.intervalTimer = intervalTimer
		self.currentState = initialState
		self.states = subsequentStates
		self.loop = loop
		if autostart {
			subscribe()
		}
	}
	
	//MARK: Publishing
	private func subscribe() {
		intervalTimer
			.receive(on: DispatchQueue.main)
			.sink { _ in
				self.publishCancelOrLoop()
		}.store(in: &cancellables)
	}
	
	private func publishCancelOrLoop() {
		if index < states.count {
			currentState = nextValue()
		} else if index >= states.count && loop {
			index = 0
			publishCancelOrLoop()
		} else {
			cancel()
			didFinish = true
		}
	}
	
	private func nextValue() -> T {
		let nextValue = self.states[self.index]
		self.index += 1
		return nextValue
	}
	
	public func start() {
		subscribe()
	}
	
	private func cancel() {
		for cancellable in cancellables {
			cancellable.cancel()
		}
	}
}
