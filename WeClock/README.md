# Studies

## Daily Movement
"Daily Movement" tracks steps, exercise minutes, stand minutes and walking distance. Apple Watch is required for exercise and stand minutes.

The data is sourced from HealthKit and tracked automatically by the system if the user has step tracking enabled. WeClock doesn't own or manage this data, it merely reads it. The relevant data type is `HKQuantitySample`.
 
 ## Locations
 "Locations and Distance" logs locations throughout the day and tracks total distance travelled. 

The data is logged manually by subscribing to updates from the Core Location visits service. `StoredLocation` objects are persisted in Core Data.

 ## Work Log
 "Work Log" records user experiences in the workplace, in the form of both notes and survey results.

The data is entered manually by the user, stored as `WorkLogEntry` objects and persisted in Core Data.

 ## Commute Time
 "Commute Time" tracks the time spent commuting. 

The data is sourced from an `HKWorkoutSession` which is initiated by the user from their Apple Watch. The resulting `HKWorkout` is saved in HealthKit. 

 ## Environmental Noise
"Environmental Noise" tracks the ambient sound levels of the user's environment. This study currently requires sensors only available on Apple Watch.

The data is sourced from HealthKit and tracked automatically by the system if the user has environmental noise tracking enabled on their Apple Watch. The relevant data type is `HKQuantitySample`.

 ## Battery Usage
 "Battery Usage" tracks estimated daily battery use. 

The data is recorded manually by WeClock. A reading is taken whenever the user opens the app. Background readings are also scheduled as part of periodic iOS background fetches. These fetches are unpredictable and controlled by the system. Readings are stored as `BatteryReading` objects and persisted in Core Data.

#  Architecture of a Study

## Study Profiles

Every study must provide a `StudyProfile` object with the following properties:

- Display name - The display name for the UI.
- Icon - The system icon displayed in the study picker.
- Tint color - The tint that will be applied to the icon.
- Opted In - A `SharedSetting` object for storing the user's opt-in preference
- Filterable - A data store which conforms to `Filterable`
- Data Store - A data store which conforms to `StudyConvertibleStore`
- Summary Provider - A data store which conforms to `SummaryProvider`
- Today - A closure which configures a view controller for the study's daily data.

The study picker UI displays the available study profiles in a table view. 

## Study Result Convertibles

Studies do not all have the same type of data. Daily Movement reads HealthKit objects, Locations and Distances stores modified `CLLocation` objects, Work Log stores notes and workplace information, etc.

The `StudyResultConvertible` protocol is a shared interface for various data types. By conforming to this type, disparate objects can be translated into a unified `StudyResult` type.  The following types already conform: 

 - `HKWorkout`
 - `HKQuantitySample`
 -  `StoredLocation`
 - `WorkLogEntry`
 - `BatteryReading`

## Study Results

Study results are meant to be displayed in a table view or exported to a CSV. They have a standardized format which mirrors the Android implementation.

## Summary Provider

A `SummaryProvider` is a data store which publishes a summary of its data in the form of a string. Summary providers are the source of the summary computations on the cards in the study picker UI. Any data store can conform to this protocol by supplying the necessary passthrough subject.

## Study Store

The `StudyStore` protocol is a composite of `Filterable` and  `StudyConvertibleStore`. 

It's a comprehensive interface for a data store which supports functionality throughout the application. The filterable conformance allows the data to be filtered by date or time period. The convertible store conformance publishes the study data as `StudyResultConvertible` objects for exporting.

Some data stores conform to `StudyStore` itself. Others only conform to one if its constitutent protocols. 

## Filterable, Searchable, Deletable

All studies support filtering, but not all studies support search or deletion. Studies which use system-collected HealthKit data do not support deletion, because WeClock cannot delete health data it did not record. Similarly, some studies – such as Battery Usage and Daily Movement – have no relevant searchable content, and so do not support search.

Study data stores that support search conform to `Searchable` and automatically display a search bar in their data table.

Study data stores that support deletion conform to `Deletable` and automatically display a delete button in their navigation bar.

All study data stores conform to `Filterable`.
