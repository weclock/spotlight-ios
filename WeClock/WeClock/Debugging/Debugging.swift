import HealthKit

class Debug {
	
	public static func save(steps: Double) {
		let store = HKModel.shared.store
		let type = HKQuantityType.quantityType(forIdentifier: .stepCount)!
		let quantity = HKQuantity(unit: .count(), doubleValue: steps)
		let sample = HKQuantitySample(type: type, quantity: quantity, start: Date(), end: Date())
		store.save(sample) { (success, error) in
			/*
			if let error = error {
				debug("Error saving step sample :(", data: error)
			}
			
			if success { debug("Saved step sample!", data: steps) }*/
		}
	}
}
