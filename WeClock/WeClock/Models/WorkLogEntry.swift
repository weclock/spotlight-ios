import Foundation
import CoreData

extension WorkLogEntry {
	
	public static func from(userInfo: [String: Any], in context: NSManagedObjectContext) -> WorkLogEntry? {
		guard let userInfoDate = userInfo["dateRecorded"] as? Date,
		let userInfoEventType = userInfo["eventType"] as? String else {
			debug("Couldn't decode date/type in user info", data: userInfo)
			return nil
		}
		let newEntry = WorkLogEntry(context: context)
		newEntry.dateRecorded = userInfoDate
		newEntry.notes = nil
		newEntry.eventType = userInfoEventType
		newEntry.source = "Apple Watch"
		return newEntry
	}
}
