import Foundation
import UIKit

extension BatteryReading {
	
	static func takeReading() {
		if !Settings.shared.batteryLevelsOptedIn.object { return } /// stop if user isn't opted in
		
		let device = UIDevice.current
		let context = CoreData.shared.viewContext
		device.isBatteryMonitoringEnabled = true
		
		let newReading = BatteryReading(context: context)
		newReading.level = device.batteryLevel
		newReading.date = Date()
		newReading.state = Int16(device.batteryState.rawValue)
		newReading.source = "foreground"
		
		try? context.save()
	}
	
	static func takeBackgroundReading() {
		if !Settings.shared.batteryLevelsOptedIn.object { return } /// stop if user isn't opted in
		
		let device = UIDevice.current
		let context = CoreData.shared.backgroundContext
		device.isBatteryMonitoringEnabled = true
		
		let newReading = BatteryReading(context: context)
		newReading.level = device.batteryLevel
		newReading.date = Date()
		newReading.state = Int16(device.batteryState.rawValue)
		newReading.source = "background"
		
		try? context.save()
	}
}

extension Array where Element == BatteryReading {
	
	var averageLevel: Double {
		let sum = reduce(into: 0) { $0 += $1.level }
		return Double(sum) / Double(count)
	}
	
	var earliestLevel: Double {
		if isEmpty { return 0 }
		
		let sorted = self.sorted { a, b in
			a.date ?? Date() > b.date ?? Date()
		}
		
		return Double(sorted.last!.level)
	}
	
	var latestLevel: Double {
		if isEmpty { return 0 }
		
		let sorted = self.sorted { a, b in
			a.date ?? Date() < b.date ?? Date()
		}
		
		return Double(sorted.last!.level)
	}
	
	var remainingLevel: Double {
		latestLevel
	}
	
	var usage: Double {
		earliestLevel - latestLevel
	}
	
	var workdayStartLevel: Double? {
		if isEmpty { return nil } /// make sure there are readings
		
		/// unwrap workday start time
		guard let workdayStart = Settings.shared.workdayStartTime else {
			debug("Couldn't find start time setting.")
			return nil
		}

		/// adjust start time for target date
		guard let startTime = adjusted(time: workdayStart, for: first!.date!) else {
			debug("Couldn't build start time from stored setting")
			return nil
		}

		/// find the readings taken before the start time
		let readingsBeforeStart = self.filter { $0.date! < startTime }
		
		/// find the reading closest to the start time, from the set of readings before it
		guard let reading = readingClosest(to: startTime, from: readingsBeforeStart) else {
			debug("Couldn't find closest reading.")
			return nil
		}
		
		return Double(reading.level)
	}
	
	var workdayEndLevel: Double? {
		if isEmpty { return nil } /// make sure there are readings
		
		/// unwrap workday end time
		guard let workdayEnd = Settings.shared.workdayEndTime else {
			debug("Couldn't find Workday End setting.")
			return nil
		}
		
		/// adjust end time for target date
		guard let endTime = adjusted(time: workdayEnd, for: first!.date!) else {
			debug("Couldn't build start time from stored setting")
			return nil
		}

		/// find the readings taken after the workday end time
		let readingsAfterEnd = self.filter { $0.date! > endTime }
		
		/// find the reading closest to the end time, from the set of readings after it
		guard let reading = readingClosest(to: endTime, from: readingsAfterEnd) else {
			debug("Couldn't find closest reading.")
			return nil
		}
	
		return Double(reading.level)
	}
	
	var workdayUsage: Double? {
		guard let startLevel = workdayStartLevel, let endLevel = workdayEndLevel else {
			debug("Couldn't find both levels.")
			return nil
		}
		
		return startLevel - endLevel
	}
	
	private func readingClosest(to date: Date, from readings: [BatteryReading]) -> BatteryReading? {
		let sorted = readings.sorted { (a, b) -> Bool in
			let intervalA = date.distance(to: a.date!)
			let intervalB = date.distance(to: b.date!)
			return intervalA < intervalB
		}
		return sorted.first
	}
	
	private func adjusted(time: Date, for referenceDate: Date) -> Date? {
		let startComponents = Calendar.current.dateComponents([.hour, .minute], from: time)
		let todayStart = Calendar.current.startOfDay(for: referenceDate)
		let startTime = Calendar.current.date(byAdding: startComponents, to: todayStart)
		return startTime
	}
}
