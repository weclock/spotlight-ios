import Foundation
import HealthKit
import Combine

class DailyBatteryViewModel:  ObservableObject, SummaryProvider, Filterable {
	
	//MARK: Observables
	@Published var averageDecibels: Double = 0 {
		didSet { summaryDecription = "\(averageDecibels.string(roundedTo: .onePlace)) decibels" }
	}
	@Published var maximumDecibels: Double = 0
	
	//MARK: Summary Provider
	@Published var summaryDecription: String = "0 decibels"
	var descriptionPublisher: Published<String>.Publisher { $summaryDecription }
	
	//MARK: Initializer
	internal init(timePeriod: HKModel.TimePeriod = .today) {
		filter(by: timePeriod)
	}
	
	private func fetch(timePeriod: HKModel.TimePeriod) {
		HKModel.Queries.audioExposure(
			timePeriod: timePeriod,
			options: .discreteAverage) { (stats) in
				let avgDecibels = stats.averageQuantity()?.doubleValue(for: .decibelAWeightedSoundPressureLevel())
				self.averageDecibels = avgDecibels ?? 0
		}?.execute()
		
		HKModel.Queries.audioExposure(
			timePeriod: timePeriod,
			options: .discreteMax) { (stats) in
				let maxDecibels = stats.maximumQuantity()?.doubleValue(for: .decibelAWeightedSoundPressureLevel())
				self.maximumDecibels = maxDecibels ?? 0
		}?.execute()
	}
	
	//MARK: Filterable
	@Published public var storedFilterDate: Date?
	public var filtering = CurrentValueSubject<Bool, Never>(false)
	public var publishedFilterDate: Published<Date?>.Publisher { $storedFilterDate }
	
	func filter(by timePeriod: HKModel.TimePeriod) {
		filtering.send(true)
		updateFilterDate(for: timePeriod)
		fetch(timePeriod: timePeriod)
	}
	
	func unfilter() {
		filtering.send(false)
		fetch(timePeriod: .any)
	}
	
	private func updateFilterDate(for timePeriod: HKModel.TimePeriod) {
		switch timePeriod {
			case .specificDay(day: let date): storedFilterDate = date
			case .today: storedFilterDate = Date()
			default: storedFilterDate = nil
		}
	}
	
	func update() {}
	
	func reset() {
		update()
	}
	
}
