import Combine
import HealthKit
import CoreLocation

class CommuteStore: ObservableObject, SummaryProvider, StudyStore, Deletable {
	
	//MARK: Convertibles
	var convertibles = CurrentValueSubject<[StudyResultConvertible], Never>([])
	
	//MARK: Published Summaries
	@Published var totalDuration: Double = 0 {
		didSet { summaryDescription = "\((totalDuration / 60).string(roundedTo: .noDecimals)) minutes" }
	}
	@Published var totalDistance: Double = 0
	@Published var totalEnergy: Double = 0
	@Published var totalFlightsClimbed: Double = 0
	@Published var averageBPM: Double = 0
	@Published var commuteRoutes = [[CLLocation]]()
	@Published var workouts = [HKWorkout]()
	var routesSubject = CurrentValueSubject<[CLLocation], Never>([])
	
	//MARK: Summary Provider
	@Published public var summaryDescription: String = String()
	public var descriptionPublisher: Published<String>.Publisher { $summaryDescription }
	
	//MARK: Initialization
	internal init(timePeriod: HKModel.TimePeriod = .today) {
		self.fetch(timePeriod: timePeriod)
	}
	
	public func fetch(timePeriod: HKModel.TimePeriod) {
		let workoutsQuery = HKModel.Queries.commuteWorkouts(timePeriod: timePeriod, completion: { (workouts) in
			self.workouts = workouts
			self.convertibles.send(workouts)
			
			self.totalDuration = workouts.reduce(into: 0) { (total, workout) in
				total += workout.duration
			}
			self.totalDistance = workouts.reduce(into: 0) { (total, workout) in
				total += workout.totalDistance?.doubleValue(for: .meter()) ?? 0
			}
			self.totalEnergy = workouts.reduce(into: 0) { (total, workout) in
				total += workout.totalEnergyBurned?.doubleValue(for: .kilocalorie()) ?? 0
			}
			self.totalFlightsClimbed = workouts.reduce(into: 0, { (total, workout) in
				total += workout.totalFlightsClimbed?.doubleValue(for: .count()) ?? 0
			})
			
			for workout in workouts {
				self.fetchHeartRate(from: workout)
				self.fetchRoute(from: workout)
			}
		}) {
			//on deletion event
			self.fetch(timePeriod: timePeriod)
		}
		
		HKModel.shared.execute(workoutsQuery)
	}
	
	//MARK: Secondary Fetches
	private func fetchHeartRate(from workout: HKWorkout) {
		let heartRateType = HKQuantityType.quantityType(forIdentifier: .heartRate)!
		let predicate = HKQuery.predicateForObjects(from: workout)
		let heartRateQuery = HKStatisticsQuery(quantityType: heartRateType, quantitySamplePredicate: predicate, options: .discreteAverage) { (_, stats, error) in
			if let error = error { debug("Error fetching heart rate stats", data: error)}
			if let stats = stats {
				let bpm = stats.averageQuantity()?.doubleValue(for: HKUnit(from: "count/min"))
				self.averageBPM = bpm ?? 0
			}
		}
		HKModel.shared.execute(heartRateQuery)
	}
	
	private func fetchRoute(from workout: HKWorkout) {
		HKModel.Queries.route(from: workout) { (routes) in
			for route in routes {
				HKModel.Queries.locations(from: route) { (locations) in
					self.routesSubject.send(locations)
				}.execute()
			}
		}.execute()
	}
	
	//MARK: Filterable
	var filtering = CurrentValueSubject<Bool, Never>(false)
	@Published public var storedFilterDate: Date?
	public var publishedFilterDate: Published<Date?>.Publisher { $storedFilterDate }
	
	func filter(by timePeriod: HKModel.TimePeriod) {
		filtering.send(true)
		updateFilterDate(for: timePeriod)
		fetch(timePeriod: timePeriod)
	}
	
	func unfilter() {
		filtering.send(false)
		fetch(timePeriod: .any)
	}
	
	private func updateFilterDate(for timePeriod: HKModel.TimePeriod) {
		switch timePeriod {
			case .specificDay(day: let date): storedFilterDate = date
			case .today: storedFilterDate = Date()
			default: storedFilterDate = nil
		}
	}

	func update() {
		fetch(timePeriod: .today)
	}
	
	func reset() {
		update()
	}
	
	func delete() {
		HKModel.shared.store.delete(workouts) { (success, errorOrNil) in
			if let error = errorOrNil { debug("Deletion error", data: error) }
			if success { debug("Successfully deleted.") }
		}
	}
	
}
