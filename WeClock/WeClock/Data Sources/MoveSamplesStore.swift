import HealthKit
import Combine
import UIKit

class MoveSamplesStore: ObservableObject, StudyStore {
	
	//MARK: Observables
	public var convertibles = CurrentValueSubject<[StudyResultConvertible], Never>([])
	
	//MARK: Properties
	private var cancellables = [AnyCancellable]()

	//MARK: Initialization
	internal init(timePeriod: HKModel.TimePeriod? = nil) {
		if let timePeriod = timePeriod { ///Dont fetch without a time period
			fetch(timePeriod: timePeriod)
		}
	}
	
	//MARK: HealthKit Interface
	public func fetch(timePeriod: HKModel.TimePeriod) {
		cancellables.forEach { $0.cancel() }
		
		let stepsFuture = HKModel.QueryBuilder.future(identifier: .stepCount, timePeriod: timePeriod)!
		
		let standFuture = HKModel.QueryBuilder.future(identifier: .appleStandTime, timePeriod: timePeriod)!
		
		let exerciseFuture = HKModel.QueryBuilder.future(identifier: .appleExerciseTime, timePeriod: timePeriod)!
		
		let distanceFuture = HKModel.QueryBuilder.future(identifier: .distanceWalkingRunning, timePeriod: timePeriod)!
		
		stepsFuture.zip(standFuture, exerciseFuture, distanceFuture)
			.receive(on: DispatchQueue(label: "backgroundWork")) /// don't block the UI
			.map { $0.0 + $0.1 + $0.2 + $0.3 } /// concatenate the arrays
			.map { $0.sorted { a, b in a.startDate > b.startDate } } /// sort samples by date
			.sink { self.convertibles.send($0) }
		.store(in: &cancellables)
	}
	
	//MARK: Filterable
	public var filtering = CurrentValueSubject<Bool, Never>(false)
	@Published public var storedFilterDate: Date?
	public var publishedFilterDate: Published<Date?>.Publisher { $storedFilterDate }
	
	public func filter(by timePeriod: HKModel.TimePeriod) {
		filtering.send(true)
		fetch(timePeriod: timePeriod)
	}
	
	func unfilter() {
		filtering.send(false)
		fetch(timePeriod: .any)
	}
	
	func update() {
		fetch(timePeriod: .thisWeek)
	}
	
	func reset() {
		update()
	}
}
