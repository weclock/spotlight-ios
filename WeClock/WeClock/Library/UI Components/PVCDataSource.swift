import UIKit

class PVCDataSource: NSObject, UIPageViewControllerDataSource {
	
	var pages: [UIViewController] = []
	
	var startPage: [UIViewController] {
		[pages[0]]
	}
	
	//MARK: PageViewController Data Source
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of: viewController)!
        let previousIndex = currentIndex - 1
        if previousIndex < 0 { return nil } //stop when index is 0
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of: viewController)!
        let nextIndex = currentIndex + 1
        if nextIndex >= pages.count { return nil } //stop when index is maxed
        return pages[nextIndex]
    }
	
}
