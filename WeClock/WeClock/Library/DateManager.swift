import Foundation

class DateManager {
	
	//MARK: Shared Instance
	public static var shared = DateManager()
	
	//MARK: Formatting
	public lazy var shortFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.calendar = Calendar.current
		formatter.locale = Calendar.current.locale
		formatter.dateStyle = .short
		formatter.timeStyle = .short
		return formatter
	}()
	
	public func shortString(for date: Date?) -> String {
		if let date = date {
			return DateManager.shared.shortFormatter.string(from: date)
		} else {
			return "Undated"
		}
	}
}
