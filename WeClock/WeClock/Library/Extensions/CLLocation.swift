import Foundation
import CoreLocation

extension CLLocation {
	
	public func placemark(_ block: @escaping (CLPlacemark) -> Void) {
		CLGeocoder().reverseGeocodeLocation(self) { (placemarksOrNil, errorOrNil) in
			if let error = errorOrNil { debug("Error geocoding", data: error) }
			
			if let placemark = placemarksOrNil?.first {
				DispatchQueue.main.async {
					block(placemark)
				}
			}
		}
	}
}
