import Foundation
import CoreData

class WatchTransferManager {
	
	let backgroundContext = CoreData.shared.backgroundContext
	
	//MARK: Saving Transfers
	public func save(_ userInfo: [String: Any]) {
		save(workLogUserInfo: userInfo)
	}
	
	public func save(workLogUserInfo: [String: Any]) {
		let entry = WorkLogEntry.from(userInfo: workLogUserInfo, in: backgroundContext)
		entry?.saveContext()
	}
}
