import Foundation
import JTAppleCalendar

extension JTACMonthView {
	
	public func selectNextDay() {
		if let tomorrow = self.selectedDates.first?.tomorrow {
			self.selectDates([tomorrow], triggerSelectionDelegate: true, keepSelectionIfMultiSelectionAllowed: false)
		}
	}
	
	public func selectPreviousDay() {
		if let yesterday = self.selectedDates.first?.yesterday {
			self.selectDates([yesterday], triggerSelectionDelegate: true, keepSelectionIfMultiSelectionAllowed: false)
		}
	}
}
