import Foundation

extension Date {
	
	public var tomorrow: Date? {
		let calendar = Calendar.current
		return calendar.date(byAdding: DateComponents(day: 1), to: self)
	}
	
	public var yesterday: Date? {
		Calendar.current.date(byAdding: DateComponents(day: -1), to: self)
	}
	
	var titleDescription: String {
		let formatter = DateFormatter()
		formatter.calendar = Calendar.current
		formatter.dateStyle = .medium
		formatter.timeStyle = .none
		return formatter.string(from: self)
	}
}

