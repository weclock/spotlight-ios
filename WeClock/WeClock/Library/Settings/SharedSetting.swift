import Foundation
import SwiftUI

class SharedSetting<O>: ObservableObject {
	
	//MARK: Initializer
	internal init(key: String, defaultValue: O, sideEffect: ((O) -> Void)? = nil) {
		self.key = key
		self.defaultValue = defaultValue
		self.object = (UserDefaults.standard.object(forKey: key) as? O) ?? defaultValue
		self.sideEffect = sideEffect
	}
	
	//MARK: Object
	var key: String
	var defaultValue: O
	
	//MARK: Observables
	@Published public var object: O {
		didSet { UserDefaults.standard.set(object, forKey: key); sideEffect?(object) }
	}
	
	//MARK: Binding Provider
	public lazy var binding: Binding<O> = {
		Binding(get: {
			return self.object
		}) { (newValue) in
			self.object = newValue
		}
	}()
	
	//MARK: Side Effects
	var sideEffect: ((O) -> Void)?
}

@propertyWrapper
struct Shared<T> {
	
	//MARK: Properties
	var shared: SharedSetting<T>
	
	//MARK: Initializer
	internal init(key: String, defaultValue: T, sideEffect: ((T) -> Void)? = nil) {
		shared = SharedSetting(key: key, defaultValue: defaultValue, sideEffect: sideEffect)
	}
	
	//MARK: Property Wrapper
	public var wrappedValue: SharedSetting<T> {
		get { shared }
    }
}
