import UIKit
import CoreLocation
import WatchConnectivity
import BackgroundTasks

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
	
	//MARK: Properties
	let locationServices = LocationServices.shared
	let watchConnectivityManager = WatchConnectivityManager.shared
	
	//MARK: UIApplication Delegate
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		
		///TestFlight only
		configureNewBuild()
		
		///Background refresh takes battery readings
		registerBackgroundRefresh()
		
		/// Start location services if authorized
		switch CLLocationManager.authorizationStatus() {
			case .authorizedAlways:
				locationServices.configure()
				locationServices.start(service: .visits)
			case .authorizedWhenInUse:
				locationServices.configure()
				locationServices.start(service: .visits)
			default: return true
		}
		
		Settings.shared.workLocation.placemark { (placemark) in
			print("work: \(placemark.name as Any)")
		}
		
		Settings.shared.homeLocation.placemark { (placemark) in
			print("home: \(placemark.name as Any)")
		}
		
		return true
	}
	
	/// For TestFlight only. Production should be opted-out by default
	func configureNewBuild() {
		if Settings.shared.lastRecordedVersion.object < 81 {
			for profile in StudyProfiles.all {
				profile.optedIn.object = true
			}
			Settings.shared.allHealthDataOptedIn.object = true
			Settings.shared.lastRecordedVersion.object = 81
		}
	}
	
	//MARK: Background Refresh
	private func registerBackgroundRefresh() {
		let success = BGTaskScheduler.shared.register(
			forTaskWithIdentifier: "com.justinkgibbons.spotlight.battery",
			using: nil) { (task) in
			self.handleBackgroundRefresh(with: task as! BGAppRefreshTask)
		}
		
		if success {
			debug("Background task registered!")
		}
	}
	
	private func handleBackgroundRefresh(with task: BGAppRefreshTask) {
		
		/// Take battery reading
		UIDevice.current.isBatteryMonitoringEnabled = true
		let reading = BatteryReading(context: CoreData.shared.viewContext)
		reading.level = UIDevice.current.batteryLevel
		reading.state = Int16(UIDevice.current.batteryState.rawValue)
		reading.source = "background"
		reading.date = Date()
		
		do {
			try CoreData.shared.viewContext.save()
			scheduleBackgroundRefresh()
			task.setTaskCompleted(success: true)
		} catch {
			scheduleBackgroundRefresh()
			task.setTaskCompleted(success: true)
		}
		
		task.expirationHandler = {
			print("expired.") /// do nothing
		}
	}
	
	public func scheduleBackgroundRefresh() {
		let request = BGAppRefreshTaskRequest(identifier: "com.justinkgibbons.spotlight.battery")
		request.earliestBeginDate = Date(timeIntervalSinceNow: 10 * 60)
		
		do {
			try BGTaskScheduler.shared.submit(request)
		} catch {
			print("Could not schedule app refresh: \(error)")
		}
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		debug("App will terminate", save: true)
	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		debug("App will resign active", save: true)
	}
	
	func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
		debug("Did receive memory warning", save: true)
	}

	// MARK: UISceneSession Lifecycle
	func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
		return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
	}

	func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
	}
}
