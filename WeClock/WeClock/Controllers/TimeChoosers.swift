import SwiftUI

struct WorkdayStartPicker: View {
	
	@Environment(\.presentationMode) var presentationMode
	@State var selectedDate: Date = Settings.shared.workdayStartTime ?? Date()
	
	//MARK: Initialization
	var body: some View {
		VStack(alignment: .center, spacing: 25) {
			Text("Start Time")
				.font(Font.system(.title))
				.bold()
			DatePicker(selection: $selectedDate, displayedComponents: [.hourAndMinute]) {
				Text("Start Time")
			}.labelsHidden()
			Button(action: {
				Settings.shared.workdayStartTime =  self.selectedDate
				self.presentationMode.wrappedValue.dismiss()
			}) {
				Text("Choose!")
			}
			.padding(12)
			.foregroundColor(.white)
			.background(Color.blue)
			.cornerRadius(10)
		}
		.frame(maxWidth: .infinity, maxHeight: .infinity)
		.background(Color("background"))
		.edgesIgnoringSafeArea(.all)
	}
}

struct WorkdayEndPicker: View {
	
	@Environment(\.presentationMode) var presentationMode
	@State var selectedDate: Date = Settings.shared.workdayEndTime ?? Date()
	
	//MARK: Initialization
	var body: some View {
		VStack(alignment: .center, spacing: 25) {
			Text("End Time")
				.font(Font.system(.title))
				.bold()
			DatePicker(selection: $selectedDate, displayedComponents: [.hourAndMinute]) {
				Text("End Time")
			}.labelsHidden()
			Button(action: {
				Settings.shared.workdayEndTime =  self.selectedDate
				self.presentationMode.wrappedValue.dismiss()
			}) {
				Text("Choose!")
			}
			.padding(12)
			.foregroundColor(.white)
			.background(Color.blue)
			.cornerRadius(10)
		}
		.frame(maxWidth: .infinity, maxHeight: .infinity)
		.background(Color("background"))
		.edgesIgnoringSafeArea(.all)
	}
}

