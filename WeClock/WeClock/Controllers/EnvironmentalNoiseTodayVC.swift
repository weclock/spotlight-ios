import UIKit
import Combine

class EnvironmentalNoiseTodayVC: UIViewController {
	
	//MARK: IBOutlets
	@IBOutlet weak var averageDecibelsLabel: BoldLabel!
	@IBOutlet weak var maxDecibelsLabel: BoldLabel!
	@IBOutlet weak var dateLabel: UILabel!
	
	//MARK: IBActions
	@IBAction func leftButtonTapped(_ sender: Any) {
		if let yesterday = viewModel.storedFilterDate?.yesterday {
			viewModel.filter(by: .specificDay(day: yesterday))
		}
	}
	
	@IBAction func rightButtonTapped(_ sender: Any) {
		if let tomorrow = viewModel.storedFilterDate?.tomorrow {
			viewModel.filter(by: .specificDay(day: tomorrow))
		}
	}
	
	//MARK: View Model
	public var viewModel: Filterable!
	private var cancellables = [AnyCancellable]()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		bind()
	}
	
	private func bind() {
		guard let noiseStore = viewModel as? EnvironmentalNoiseViewModel else {
			debug("Coulnd't typecast environmental noise vm.")
			return
		}
		
		noiseStore.$averageDecibels
			.receive(on: DispatchQueue.main)
			.map { $0.string(roundedTo: .onePlace) }
			.sink { self.averageDecibelsLabel.text = $0 }
		.store(in: &cancellables)
		
		noiseStore.$maximumDecibels
			.receive(on: DispatchQueue.main)
			.map { $0.string(roundedTo: .onePlace) }
			.sink { self.maxDecibelsLabel.text = $0 }
		.store(in: &cancellables)
		
		///Receive updates from calendar
		noiseStore.publishedFilterDate
			.receive(on: DispatchQueue.main)
			.map { $0?.titleDescription ?? "No Date" }
			.sink { self.dateLabel.text = $0}
		.store(in: &cancellables)
	}
}
