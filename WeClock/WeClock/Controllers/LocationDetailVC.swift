import UIKit
import Combine

class LocationDetailVC: UIViewController {
	
	//MARK: IBOutlets
	@IBOutlet weak var streetAddressLabel: UILabel!
	@IBOutlet weak var cityLabel: UILabel!
	@IBOutlet weak var stateLabel: UILabel!
	@IBOutlet weak var countryLabel: UILabel!
	@IBOutlet weak var dateLabel: UILabel!
	@IBOutlet weak var horizontalAccuracyLabel: UILabel!
	@IBOutlet weak var serviceTypeLabel: UILabel!
	@IBOutlet weak var mapContainer: UIView!
	@IBOutlet weak var accuracyClassLabel: UILabel!
	@IBOutlet weak var latitudeLabel: UILabel!
	@IBOutlet weak var longitudeLabel: UILabel!
	
	//MARK: IBActions
	@IBAction func deleteButtonTapped(_ sender: Any) {
		let context = CoreData.shared.viewContext
		context.delete(location)
		try? context.save()
		navigationController?.popViewController(animated: true)
	}
	
	//MARK: Properties
	public var location: StoredLocation!
	private let mapComponent = MapComponent()
	private var cancellables = [AnyCancellable]()
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		bind()
		
		mapComponent.snap(to: mapContainer)
		mapComponent.showLoading()
		
		location.addPlacemarkDetailsIfMissing() {
			self.mapComponent.showMap()
		}
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
		CoreData.shared.saveContext() //Commit any placemark details that were downloaded
	}
	
	//MARK: View Model
	func bind() {
		location.publisher(for: \.placeName)
			.sink {
				self.streetAddressLabel.text = $0
				self.mapComponent.mapView.dropPin(at: self.location.flatCLLocation, title: $0)
				self.mapComponent.mapView.center(at: self.location.coordinates, radius: 500)
			}
		.store(in: &cancellables)
		location.publisher(for: \.cityName)
			.sink { self.cityLabel.text = $0 }
		.store(in: &cancellables)
		location.publisher(for: \.stateAbbreviation)
			.sink { self.stateLabel.text = $0 }
		.store(in: &cancellables)
		location.publisher(for: \.countryName)
			.sink { self.countryLabel.text = $0 }
		.store(in: &cancellables)
		location.publisher(for: \.dateVisited)
			.map { $0?.description(with: Calendar.current.locale) }
			.sink { self.dateLabel.text = $0 }
		.store(in: &cancellables)
		location.publisher(for: \.horizontalAccuracy)
			.map { $0.string(roundedTo: .onePlace) }
			.sink { self.horizontalAccuracyLabel.text = $0 }
		.store(in: &cancellables)
		location.publisher(for: \.clServiceSource)
			.sink { self.serviceTypeLabel.text = $0 ?? "Unknown service" }
		.store(in: &cancellables)
		location.publisher(for: \.latitude)
			.map { String($0) }
			.sink { self.latitudeLabel.text = $0 }
		.store(in: &cancellables)
		location.publisher(for: \.longitude)
			.map { String($0) }
			.sink { self.longitudeLabel.text = $0 }
		.store(in: &cancellables)
		location.publisher(for: \.horizontalAccuracy)
			.map { $0 < 200 }
			.map { $0 ? "Included in daily distance." : "Ignored as inaccurate." }
			.sink {
				self.accuracyClassLabel.text = $0
				if self.location.isSufficientlyAccurate { self.accuracyClassLabel.textColor = .systemGreen }
				else { self.accuracyClassLabel.textColor = .secondaryLabel }
			}
		.store(in: &cancellables)
	}

}
