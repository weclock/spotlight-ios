import UIKit
import Combine

class StudyProfileTableViewCell: UITableViewCell {
	
	//MARK: IBOutlets
	@IBOutlet weak var studyNameLabel: UILabel!
	@IBOutlet weak var card: UXView!
	@IBOutlet weak var icon: UIImageView!
	@IBOutlet weak var summaryPreviewLabel: UILabel!
	@IBOutlet weak var optInOutButton: UIButton!
	
	//MARK: IBActions
	@IBAction func optInOutButtonTapped(_ sender: Any) {
		if profile.optedIn.object {
			profile.optedIn.object = false
		} else {
			profile.optedIn.object = true
		}
	}
	
	//MARK: View Model
	public var profile: StudyProfile! {
		didSet { bindProfile() }
	}
	public var tableViewModel: StudyPickerViewModel! {
		didSet { bind() }
	}
	private var cancellables = [AnyCancellable]()
	
	public func bind() {
		tableViewModel.$isEditing
			.sink {
				self.adjustOptButtonAlpha($0 ? 1 : 0 )
				self.adjustSummaryLabelAlpha($0 ? 0 : 1)
		}
		.store(in: &cancellables)
	}
	
	public func bindProfile() {
		profile.optedIn.$object
			.sink { self.changeOptButtonImage(state: $0) }
		.store(in: &cancellables)
		
		profile.summaryStore.descriptionPublisher
			.receive(on: DispatchQueue.main)
			.sink { self.summaryPreviewLabel.text = $0 }
		.store(in: &cancellables)
	}
	
	private func adjustOptButtonAlpha(_ value: CGFloat) {
		UIView.animate(withDuration: 0.15) {
			self.optInOutButton.alpha = value
		}
	}
	
	private func adjustSummaryLabelAlpha(_ value: CGFloat) {
		UIView.animate(withDuration: 0.15) {
			self.summaryPreviewLabel.alpha = value
		}
	}

	private func changeOptButtonImage(state: Bool) {
		let optImage = UIImage(systemName: state ? "minus.circle.fill" : "plus.circle.fill")!
		let optColor: UIColor = state ? .systemRed : .systemGreen
		UIView.animate(withDuration: 0.15) {
			self.optInOutButton.setBackgroundImage(optImage, for: .normal)
			self.optInOutButton.tintColor = optColor
		}
	}
	
	public func configure(for profile: StudyProfile) {
		icon.image = UIImage(systemName: profile.iconName)
		icon.tintColor = profile.tintColor
		studyNameLabel.text = profile.displayName
		card.addShadows(color: UIColor(named: "shadow") ?? .black, opacity: 0.1, radius: 3)
		card.layer.borderColor = UIColor(named: "border")?.cgColor
		card.layer.borderWidth = 0
	}
	
	///Cancel all subscriptions before reusing cell
	override func prepareForReuse() {
		self.cancellables = []
	}
}
