import UIKit

class SurveyConsentVC: UIViewController, UIInteractionDelegate {
	
	//MARK: IBOutlets
	@IBOutlet weak var nextButton: UXButton!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		hideNavbar()
		nextButton.delegate = self
	}
	
	func hideNavbar() {
		navigationController?.navigationBar.isTranslucent = false
//		navigationController?.navigationBar.prefersLargeTitles = false
		navigationItem.largeTitleDisplayMode = .never
		navigationController?.navigationBar.barTintColor = UIColor(named: "background")
		navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
		navigationController?.navigationBar.shadowImage = UIImage()
	}
	
	//MARK: UIInteraction Delegate
	func buttonTouchUp() {
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "onboardingPageTwo")
		navigationController?.pushViewController(vc, animated: true)
	}
	
}
