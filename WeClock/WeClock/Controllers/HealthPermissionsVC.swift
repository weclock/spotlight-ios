import UIKit
import HealthKit

class HealthPermissionsVC: UIViewController, UIInteractionDelegate {
	
	//MARK: IBOutlet
	@IBOutlet weak var nextButton: UXButton!
	
	//MARK: UIViewController
	override func viewDidLoad() {
		super.viewDidLoad()
		nextButton.delegate = self
	}
	
	func buttonTouchUp() {
		authorizeHealthKit()
	}
	
	func nextPage() {
		let storyboard = UIStoryboard(name: "Onboarding", bundle: Bundle.main)
		let vc = storyboard.instantiateViewController(identifier: "locationPermissions")
		navigationController?.pushViewController(vc, animated: true)
	}
	
	private func authorizeHealthKit() {
			
		var readTypes = HKQuantityType.quantityTypes(for: [
			.stepCount,
			.distanceWalkingRunning,
			.appleExerciseTime,
			.appleStandTime,
			.heartRate,
			.basalEnergyBurned,
			.activeEnergyBurned,
			.flightsClimbed,
			.environmentalAudioExposure
		])
		
		//Write access can't include Apple proprietary types
		var writeTypes = HKQuantityType.quantityTypes(for: [
			.stepCount,
			.distanceWalkingRunning,
			.basalEnergyBurned,
			.activeEnergyBurned,
			.flightsClimbed,
			.heartRate
		])
		
		readTypes.insert(HKWorkoutType.workoutType())
		readTypes.insert(HKSeriesType.workoutRoute())
		writeTypes.insert(HKWorkoutType.workoutType())
		writeTypes.insert(HKSeriesType.workoutRoute())
		
		let hkauth = HKAuthManager(
			readTypes: readTypes,
			writeTypes: writeTypes
		)
		
		hkauth.authorize {
			DispatchQueue.main.async { self.nextPage() }
		}
		Settings.shared.hasAskedForHKPermission.object = true
	}
}
