import CoreLocation
import UIKit

class LocationServices {
	
	//MARK: Shared Instance
	static var shared: LocationServices = LocationServices()
	
	//MARK: Properties
	public let locationManager = CLLocationManager()
	public let locationDelegate = LocationDelegate()
	private var defaultService: ServiceType = .visits
	
	//MARK: Initialization
	public func configure() {
		locationManager.delegate = locationDelegate
		locationManager.requestAlwaysAuthorization()
		
		//Allow background reception
		locationManager.allowsBackgroundLocationUpdates = true
//		locationManager.pausesLocationUpdatesAutomatically = false
		locationManager.showsBackgroundLocationIndicator = true
	}
	
	func startStandardService() {
		locationManager.startUpdatingLocation()
		locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
		locationManager.distanceFilter = 50
	}
	
	func start(service: ServiceType) {
		switch service {
			case .standard: startStandardService()
			case .visits: locationManager.startMonitoringVisits()
			case .significant: locationManager.startMonitoringSignificantLocationChanges()
			case .off: print("do nothing")
		}
	}
	
	//MARK: Service Switching
	public func changeService(to newService: LocationServices.ServiceType) {
		switch newService {
			case .standard:
				locationManager.stopMonitoringVisits()
				locationManager.stopMonitoringSignificantLocationChanges()
				startStandardService()
			case .visits:
				locationManager.stopMonitoringSignificantLocationChanges()
				locationManager.stopUpdatingLocation()
				locationManager.startMonitoringVisits()
			case .significant:
				locationManager.stopUpdatingLocation()
				locationManager.stopMonitoringVisits()
				locationManager.startMonitoringSignificantLocationChanges()
			case .off:
				locationManager.stopUpdatingLocation()
				locationManager.stopMonitoringSignificantLocationChanges()
				locationManager.stopMonitoringVisits()
		}
	}
	
	//MARK: Helpers
	public func checkLaunchOptions(_ launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
		if let locationOption = launchOptions?[UIApplication.LaunchOptionsKey.location] as? NSNumber {
			debug("lanchOptions locationFound", save: true, data: locationOption)
		}
	}
	
	public func reportLatestLocation() {
		debug("reporting location imperatively", save: true)
		locationManager.requestLocation()
	}
}

/*
	Determines which Core Location service to subscribe to.
	
	.off: turn off location services
	.significant: significant change service. low accuracy, low battery
	.visits: visits service - mid accuracy, mid battery
	.standard: standard service - high accuracy, high battery
*/
extension LocationServices {
	
	enum ServiceType: Int {
		case off = 0
		case significant = 1
		case visits = 2
		case standard = 3
	}
}
