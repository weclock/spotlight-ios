import Foundation
import HealthKit

extension HKQuantitySample {
	
	public var friendlyDescription: String {
		if let unit = friendlyUnit {
			let double = self.quantity.doubleValue(for: unit)
			return "\(double.string(roundedTo: .noDecimals)) \(unit.description)"
		} else {
			return "Error decoding sample"
		}
	}
	
	public var friendlyUnit: HKUnit? {
		switch self.quantityType.identifier {
			case "HKQuantityTypeIdentifierAppleExerciseTime": return HKUnit.minute()
			case "HKQuantityTypeIdentifierStepCount": return HKUnit.count()
			case "HKQuantityTypeIdentifierAppleStandTime": return HKUnit.minute()
			case "HKQuantityTypeIdentifierDistanceWalkingRunning": return HKUnit.meter()
			case "HKQuantityTypeIdentifierEnvironmentalAudioExposure": return HKUnit.decibelAWeightedSoundPressureLevel()
			default: return nil
		}
	}
	
	public var friendlyTitle: String {
		switch self.quantityType.identifier {
			case "HKQuantityTypeIdentifierAppleExerciseTime": return "Exercise Sample"
			case "HKQuantityTypeIdentifierStepCount": return "Step Sample"
			case "HKQuantityTypeIdentifierAppleStandTime": return "Stand Sample"
			case "HKQuantityTypeIdentifierDistanceWalkingRunning": return "Distance Sample"
			case "HKQuantityTypeIdentifierEnvironmentalAudioExposure": return "Noise Sample"
			default: return "Unknown Sample"
		}
	}
}
