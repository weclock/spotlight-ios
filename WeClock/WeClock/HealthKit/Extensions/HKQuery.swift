import HealthKit

extension HKQuery {
	
	public func execute() {
		HKModel.shared.execute(self)
	}
}
