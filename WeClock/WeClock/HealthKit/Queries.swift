import HealthKit
import CoreLocation

/*
	A namespace for common HealthKit queries. They are stored as computed properties because HealthKit will throw an error if a query is re-used. Every query must be a new object, even if it's configuration is the same.
*/

extension HKModel {
	
	enum Queries {
		
		public static func todayStepsQuery(_ completion: @escaping (HKStatistics) -> Void) -> HKStatisticsQuery {
			
			let sampleType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
			let todayStart = Date.startDate(of: .today)
			let predicate = HKQuery.predicateForSamples(withStart: todayStart, end: Date(), options: [])
			
			let query = HKStatisticsQuery(quantityType: sampleType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (query, stats, error) in
				if let error = error { debug("Error executing query: \(error)") }
				
				if let stats = stats {
					completion(stats)
				}
			}
			
			return query
		}
		
		public static func allStepsQuery(_ completion: @escaping (HKStatistics) -> Void) -> HKStatisticsQuery {
			
			let sampleType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
			let predicate: NSPredicate? = nil
			
			let query = HKStatisticsQuery(quantityType: sampleType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (query, stats, error) in
				if let error = error { debug("Error executing query: \(error)") }
				
				if let stats = stats {
					completion(stats)
				}
			}
			
			return query
		}
		
		public static func exerciseMinutesToday(_ completion: @escaping (HKStatistics) -> Void) -> HKStatisticsQuery {
			
			let sampleType = HKQuantityType.quantityType(forIdentifier: .appleExerciseTime)!
			let todayStart = Date.startDate(of: .today)
			let predicate = HKQuery.predicateForSamples(withStart: todayStart, end: Date(), options: [])
			
			let query = HKStatisticsQuery(quantityType: sampleType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (query, stats, error) in
				if let error = error { debug("Error executing query: \(error)") }
				
				if let stats = stats {
					completion(stats)
				}
			}
			
			return query
		}
		
		public static func exerciseMinutesTotal(_ completion: @escaping (HKStatistics) -> Void) -> HKStatisticsQuery {
			
			let sampleType = HKQuantityType.quantityType(forIdentifier: .appleExerciseTime)!
			let predicate: NSPredicate? = nil
			
			let query = HKStatisticsQuery(quantityType: sampleType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (query, stats, error) in
				if let error = error { debug("Error executing query: \(error)") }
				
				if let stats = stats {
					completion(stats)
				}
			}
			
			return query
		}
		
		public static func standMinutesTotal(_ completion: @escaping (HKStatistics) -> Void) -> HKStatisticsQuery {
			
			let sampleType = HKQuantityType.quantityType(forIdentifier: .appleStandTime)!
			let predicate: NSPredicate? = nil
			
			let query = HKStatisticsQuery(quantityType: sampleType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (query, stats, error) in
				if let error = error { debug("Error executing query: \(error)") }
				
				if let stats = stats {
					completion(stats)
				}
			}
			
			return query
		}
		
		public static func standMinutesToday(_ completion: @escaping (HKStatistics) -> Void) -> HKStatisticsQuery {
			
			let sampleType = HKQuantityType.quantityType(forIdentifier: .appleStandTime)!
			let todayStart = Date.startDate(of: .today)
			let predicate = HKQuery.predicateForSamples(withStart: todayStart, end: Date(), options: [])
			
			let query = HKStatisticsQuery(quantityType: sampleType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (query, stats, error) in
				if let error = error { debug("Error executing query: \(error)") }
				
				if let stats = stats {
					completion(stats)
				}
			}
			return query
		}
		
		public static func commuteWorkouts(timePeriod: HKModel.TimePeriod, completion: @escaping ([HKWorkout]) -> Void, deletionHandler: (() -> Void)? = nil) -> HKAnchoredObjectQuery {
			let sampleType = HKWorkoutType.workoutType()
			var subpredicates: [NSPredicate] = []
			let metadataPredicate = HKQuery.predicateForObjects(withMetadataKey: Keys.CustomWorkoutType, allowedValues: [Identifiers.CommuteWorkout])
			let sourcePredicate = HKQuery.predicateForObjects(from: HKSource.default())
			subpredicates += [metadataPredicate, sourcePredicate]
			if  let datePredicate = HKModel.Predicate.from(timePeriod) {
				subpredicates.append(datePredicate)
			}
			let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: subpredicates)
			
			let query = HKAnchoredObjectQuery(type: sampleType, predicate: predicate, anchor: nil, limit: HKObjectQueryNoLimit) { (query, samplesOrNil, deletionsOrNil, anchor, errorOrNil) in
				if let error = errorOrNil { debug("Query error", data: error) }
				if let workouts = samplesOrNil as? [HKWorkout] {
					completion(workouts)
				}
			}
			
			query.updateHandler = { (query, samplesOrNil, deletionsOrNil, anchor, error) in
				if let deletedObjects = deletionsOrNil {
					if !deletedObjects.isEmpty { deletionHandler?() }
				}
			}
			
			return query
		}
		
		public static func commuteDistanceSamples(completion: @escaping (HKStatistics) -> Void) -> HKStatisticsQuery {
			let metadataPredicate = HKQuery.predicateForObjects(withMetadataKey: Keys.CustomWorkoutType, allowedValues: [Identifiers.CommuteWorkout])
			let datePredicate = HKModel.Predicate.from(.today)!
			let predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [metadataPredicate, datePredicate])
			let distanceType = HKQuantityType.quantityType(forIdentifier: .distanceWalkingRunning)!
			let samplesQuery = HKStatisticsQuery(quantityType: distanceType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (query, stats, error) in
				if let error = error { debug("Error with statistics query", data: error) }
				if let stats = stats {
					completion(stats)
				}
			}
			return samplesQuery
		}
		
		public static func avgHeartRate(during workout: HKWorkout, completion: @escaping (Double) -> Void) -> HKStatisticsQuery {
			let heartRateType = HKQuantityType.quantityType(forIdentifier: .heartRate)!
			let predicate = HKQuery.predicateForObjects(from: workout)
			let query = HKStatisticsQuery(quantityType: heartRateType, quantitySamplePredicate: predicate, options: .discreteAverage) { (_, stats, error) in
				if let error = error { debug("Error fetching heart rate stats", data: error)}
				if let stats = stats {
					let bpm = stats.averageQuantity()?.doubleValue(for: HKUnit(from: "count/min"))
					completion(bpm ?? 0)
				}
			}
			return query
		}
		
		public static func route(from workout: HKWorkout, completion: @escaping ([HKWorkoutRoute]) -> Void) -> HKSampleQuery {
			let predicate = HKQuery.predicateForObjects(from: workout)
			let query = HKSampleQuery(sampleType: HKSeriesType.workoutRoute(), predicate: predicate, limit: 200, sortDescriptors: [NSSortDescriptor(keyPath: \HKWorkoutRoute.startDate, ascending: false)]) { (_, samples, error) in
				if let error = error { debug("Error fetching route samples", data: error) }
				if let workoutRoutes = samples as? [HKWorkoutRoute] {
					completion(workoutRoutes)
				}
			}
			return query
		}
		
		public static func locations(from route: HKWorkoutRoute, completion: @escaping ([CLLocation]) -> Void) -> HKWorkoutRouteQuery {
			let query = HKWorkoutRouteQuery(route: route) { (query, locationsOrNil, done, errorOrNil) in
				
				// This block may be called multiple times.
				if let error = errorOrNil {
					debug("Error querying route data", data: error); return
				}
				
				guard let locations = locationsOrNil else {
					fatalError("*** Invalid State: This can only fail if there was an error. ***")
				}
				
				// Do something with this batch of location data.
				if done {
					completion(locations)
				}
			}
			return query
		}
		
		public static func audioExposure(timePeriod: HKModel.TimePeriod, options: HKStatisticsOptions, completion: @escaping (HKStatistics) -> Void) -> HKStatisticsCollectionQuery? {
			return HKModel.QueryBuilder.collectionQuery(identifier: .environmentalAudioExposure, timePeriod: timePeriod, options: options) { (stats) in
				completion(stats)
			}
		}
	}
}

