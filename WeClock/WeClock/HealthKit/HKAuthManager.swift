import Foundation
import HealthKit

public class HKAuthManager {
	
	//MARK: Initialization
	init(readTypes: Set<HKSampleType>, writeTypes: Set<HKSampleType>) {
		self.readTypes = readTypes
		self.writeTypes = writeTypes
	}
	
	private var readTypes: Set<HKSampleType>
	private var writeTypes: Set<HKSampleType>
	
	//MARK: Properties
    private let store = HKHealthStore()
	
	//MARK: Computed Properties
	public var hasWriteAuthorization: Bool {
        guard let status = status(for: .dietaryEnergyConsumed) else {
            debug("Couldn't get authorization status"); return false
        }
        switch status {
			case .notDetermined: return false
			case .sharingDenied: return false
			case .sharingAuthorized: return true
			default: return false
        }
    }
    
    public var needsWritesAuthorization: Bool {
        return !hasWriteAuthorization
    }
    
	//MARK: Methods
    public func authorize(_ onSuccess: @escaping () -> Void) {
        store.requestAuthorization(toShare: writeTypes, read: readTypes) { (success, error) in
            if let error = error { print("error", error) }
            
            if success {
				debug("HK authorized")
            }
            
            onSuccess()
        }
    }
    
    public func status(for identifier: HKQuantityTypeIdentifier) -> HKAuthorizationStatus? {
        guard let type = HKQuantityType.quantityType(forIdentifier: identifier) else {
            debug("Couldn't construct type from identifier!"); return nil
        }
        return store.authorizationStatus(for: type)
    }
    
}
